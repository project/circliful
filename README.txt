-- SUMMARY --

Incorporates the Circliful JQuery plugin so you can make
pretty round pie charts. See
http://ladensia.com/circliful/index.html

*** This is an early development version of this module ***
It will not generate these graphics for you without adding HTML to your content.


-- REQUIREMENTS --

Libraries 2.0 or later
Jquery Update : You must run JQuery version 1.10 or later.
Optional:
JQuery Waypoints - https://www.drupal.org/project/waypoints
If this module is installed, the circles will animate when scrolled to.


-- INSTALLATION --

* Download and enable the module.
* Download the JQuery Circliful library from
http://ladensia.com/circliful/index.html.
Direct download link: http://ladensia.com/circliful/index.html
* Extract the downloaded library to a folder in your libraries folder
(usually sites/all/libraries).
* rename the circliful folder to 'jquery-plugin-circliful'.
* Check the path to the circliful javascript. It should be
DOCROOT/sites/all/libraries/jquery-plugin-circliful/js/jquery.circliful.min.js
[adjust this path if your libraries folder is in a different location]

-- USAGE --

Once the module is installed, the JQuery Circlify plugin will be available
and listening on all pages. Any element with the class "circliful" will have the
plugin applied to it.

To include a Circliful graphic on a page or block, add code as per the examples
at http://ladensia.com/circliful/index.html , but add the class 'circliful' to
the container. For example:

<div class="circliful text-color-blue" data-dimension="150" data-text="50%" data-width="10" data-fontsize="36" data-percent="50" data-fgcolor="#00aeef" data-bgcolor="#80d7f7" ></div>

If you add this to a node or block, don't forget to set your input filters
correctly as the code may be filtered out.

Alternatively circliful is available as a theme function, eg
<?php
theme('circliful', array(
    'dimension' => 200,
    'text' => '35%',
    'info' => 'New Clients',
    'width' => 30,
    'fontsize' => 38,
    'percent' => 35,
    'fgcolor' => '#61a9dc',
    'bgcolor' => '#eee',
    'fill' => '#ddd',
    'type' => 'full',
  ));
?>

See the template file templates/circiful.tpl.php for information about the
variables.


--  TROUBLESHOOTING --

* Check that your embed container is actually output on the page (try viewing
the page source or inspecting with firebug or similar). If the container does
not show in the source, it may have been filtered out by your input filters.
Try changing the text format to 'full HTML'.

* If the container is present in the source, check the javascript console for
errors and log an issue!

-- CONTACT --

Current maintainers:
* Phil Dodd (tripper54) - http://drupal.org/user/452964