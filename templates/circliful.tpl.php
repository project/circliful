<?php
/**
 * Circliful template.
 * Variables available:
 * 'dimension': dimension in pixles.
 * 'text': text to display in the middle of the circle
 * 'info': appears underneath the text in a smaller type.
* 'width': width of the color stip in pixels
* 'fontsize' : size of the text font
* 'percent': percent to display ( 0  - 100)
* 'fgcolor': foreground color for the data circle
* 'bgcolor': background color for the data circle
* 'fill': background color for inside the data circle
 * 'type': full or half (defaults to full).
 *
 */
?>
<div class="circliful" data-dimension="<?php print $dimension;?>" data-text="<?php print $text;?>" data-info="<?php print $info; ?>" data-width="<?php print $width; ?>" data-fontsize="<?php print $fontsize; ?>" data-percent="<?php print $percent; ?>" data-fgcolor="<?php print $fgcolor; ?>" data-bgcolor="<?php print $bgcolor; ?>" data-type="<?php print $type; ?>" data-fill="<?php print $fill; ?>"></div>
